const coursesData = [
	{
		id: "wdc001",
		name: "PHP- Laravel",
		description: "Lorem ipsum dolor sit amet.",
		price: 45000,
		onOffer: true,
	},
	{
		id: "wdc002",
		name: "Python- Django",
		description: "Lorem ipsum dolor sit amet.",
		price: 50000,
		onOffer: true,
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit. ",
		price: 55000,
		onOffer: true,
	},
];

export default coursesData;
