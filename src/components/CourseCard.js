import { useEffect, useState } from "react";
import { Button, Card } from "react-bootstrap";

export default function CourseCard({ courseProp }) {
	// console.log(courseProp);
	// console.log(typeof props);

	// deconstruct the courseProp into their own variables
	const { name, description, price } = courseProp;

	// Syntax
	// const [getter,setter] = useState(initalValueOfGetter)
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);

	function enroll() {
		if (seats > 0) {
			setCount(count + 1);
			console.log(`Enrollees: ${count}`);
			setSeats(seats - 1);
			console.log(`Seats: ${seats}`);
		}
	}

	useEffect(() => {
		if (seats === 0) {
			alert(`No more seats available`);
		}
	}, [seats]);
	return (
		<Card className="mt-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Card.Subtitle className="mb-3">
					Enrollees: {count}
				</Card.Subtitle>
				<Button variant="primary" onClick={enroll}>
					Enroll
				</Button>{" "}
			</Card.Body>
		</Card>
	);
}
