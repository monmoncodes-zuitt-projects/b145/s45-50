import { Fragment, useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";

export default function Register() {
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setisActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	function registerUser(e) {
		e.preventDefault();

		// Clear input fields
		setEmail("");
		setPassword1("");
		setPassword2("");

		alert(`Thank you for registering!`);
	}

	useEffect(() => {
		if (
			email !== "" &&
			password1 !== "" &&
			password2 !== "" &&
			password1 === password2
		) {
			setisActive(true);
		} else {
			setisActive(false);
		}
	}, [email, password1, password2]);

	return (
		<Fragment>
			<h1>Register</h1>
			<Form onSubmit={(e) => registerUser(e)}>
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter your email here"
						value={email}
						onChange={(e) => setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
						We will never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter your password here"
						value={password1}
						onChange={(e) => setPassword1(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Re-enter your password here"
						value={password2}
						onChange={(e) => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>

				{isActive ? (
					<Button
						variant="success"
						type="submit"
						id="submitBtn"
						className="mt-3"
					>
						Submit
					</Button>
				) : (
					<Button
						variant="danger"
						type="submit"
						id="submitBtn"
						className="mt-3"
						disabled
					>
						Submit
					</Button>
				)}
			</Form>
		</Fragment>
	);
}
