import { Fragment } from "react";
import coursesData from "../data/coursesData";
import CourseCard from "../components/CourseCard";

export default function Courses() {
	const courses = coursesData.map((course) => {
		return <CourseCard key={course.id} courseProp={course} />;
	});
	// console.log(coursesData);
	// console.log(coursesData[0]);
	return (
		<Fragment>
			<h1>Courses</h1>
			{courses}
		</Fragment>
	);
}
