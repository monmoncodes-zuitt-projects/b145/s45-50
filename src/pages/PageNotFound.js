import { Link } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";

export default function PageNotFound() {
	return (
		<Row>
			<Col className="p-5 text-center">
				<h1>Page Not Found</h1>
				<p>
					Go back to <Link to="/">homepage</Link>
				</p>
			</Col>
		</Row>
	);
}
