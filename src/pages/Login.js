import { Fragment, useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";

export default function Login() {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setisActive] = useState(false);

	console.log(email);
	console.log(password);

	function loginUser(e) {
		e.preventDefault();

		// localStorage.setItem('key',value)
		localStorage.setItem("email", email);

		// Clear input fields
		setEmail("");
		setPassword("");

		alert(`You are now logged in!`);
	}

	useEffect(() => {
		if (email && password !== "") {
			setisActive(true);
		}
	}, [email, password]);

	return (
		<Fragment>
			<h1 className="mt-5">Login</h1>
			<Form onSubmit={(e) => loginUser(e)}>
				<Form.Group controlId="loginEmail">
					<Form.Label>Email Address:</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email here"
						value={email}
						onChange={(e) => setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="loginPassword">
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter password here"
						value={password}
						onChange={(e) => setPassword(e.target.value)}
						required
					/>
				</Form.Group>

				{isActive ? (
					<Button
						variant="success"
						type="submit"
						id="loginBtn"
						className="mt-3"
					>
						Login
					</Button>
				) : (
					<Button
						variant="success"
						type="submit"
						id="loginBtn"
						className="mt-3"
						disabled
					>
						Login
					</Button>
				)}
			</Form>
		</Fragment>
	);
}
